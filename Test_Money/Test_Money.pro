#-------------------------------------------------
#
# Project created by QtCreator 2019-03-18T04:09:50
#
#-------------------------------------------------

QT       += testlib

QT       -= gui

TARGET = test_moneytest
CONFIG   += console testcase
CONFIG   -= app_bundle

TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

# подключаем исходники тестирующего проекта
SOURCES += \
    main.cpp \
    test_moneytest.cpp

# подключаем исходники тестируемого проекта
# *.cpp означает то, что мы подключаем файлы с расширением .cpp
# две точки означает переход на уровень выше.
# то есть мы поднимаемся в родительскую папку, где находится тестируемый проект
# и подключаем все его исходники. Также с заголовочными файлами
SOURCES += $$files(../*.cpp)
# исключаем файл main основного проекта, чтоб не было конфликтов
SOURCES -= ../main.cpp

# подключаем заголовчники тестирующего проекта
HEADERS += \
    test_moneytest.h

# подключаем заголовчники тестируемого проекта
HEADERS += ../*.h
