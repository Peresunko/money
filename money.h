#ifndef MONEY_H
#define MONEY_H

//!  Класс Money
/*!
  Является родительским классом для класса Dollar и Franc.
  В этом классе заложена базовая функциональность денюжек.
*/


class Money
{
protected:
    int m_amount; /*!< Количество денег */
public:
    //! Конструктор класса.
    /*!
      \param amount Количество денег.
       Создает объект класса Money с определенным количеством денег.
    */
    Money(int amount);
    //! Функция для проверки идентичности денег.
    /*!
      \param m Другие деньги.
      \return Результат сравнения
    */
    bool equals(const Money& m);
};

#endif // MONEY_H
