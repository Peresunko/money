#ifndef FRANC_H
#define FRANC_H

#include "money.h"

//!  Класс Franc
/*!
  Деньги во франках
*/

class Franc : public Money
{
public:
    //! Конструктор класса.
    /*!
      \param amount Количество денег.
       Создает объект класса Franc с определенным количеством денег.
    */
    Franc(int amount);
    //! Функция для умножения количества денег на множитель.
    /*!
      \param multiplier Множитель.
      \return Объект класса Franc с умноженным количеством денег
    */
    Franc times(int multiplier);
};


#endif // FRANC_H

