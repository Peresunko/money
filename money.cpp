#include "money.h"

/*!
  \fn QAbstractTransition::triggered()

  This signal is emitted when the transition has been triggered (after
  onTransition() has been called).
*/

Money::Money(int amount)
{
    m_amount = amount;
}

bool Money::equals(const Money& m)
{
    return m.m_amount == m_amount;
}
