#ifndef DOLLAR_H
#define DOLLAR_H

#include "money.h"

//!  Класс Franc
/*!
  Деньги во франках
*/


class Dollar : public Money
{
public:
    //! Конструктор класса.
    /*!
      \param amount Количество денег.
       Создает объект класса Dollar с определенным количеством денег.
    */

    Dollar(int amount);
    //! Функция для умножения количества денег на множитель.
    /*!
      \param multiplier Множитель.
      \return Объект класса Dollar с умноженным количеством денег
    */
    Dollar times(int multiplier);
};

#endif // DOLLAR_H
